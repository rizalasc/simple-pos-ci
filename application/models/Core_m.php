<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Core_m extends CI_Model
{

    public function insert($table, $data)
    {
        return $this->db->insert($table, $data);
    }

    public function update($table, $data, $where)
    {
        return $this->db->update($table, $data, $where);
    }

    public function delete($table, $where)
    {
        return $this->db->delete($table, $where);
    }

    public function getFull($table)
    {
        return $this->db->get($table);
    }

    public function getByWhere($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    public function checkUser($table, $login)
    {
        $email = $login;
        $query = $this->db->where('username', $email)->get($table);
        return $query->row_array();
    }

    public function checkPassword($table, $user)
    {
        if (!empty($user['username'])) {
            $value = array(
                'username' => $user['username'],
                'password' => sha1($user['password'])
            );
            $query = $this->db->get_where($table, $value);
        }

        if ($query->row_array()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function checkId($table, $key, $like)
    {
        return $this->db->select_max($key)->like($key, $like, 'after')->get($table);
    }

    public function getJoin2table($select, $table1, $table2, $primary1, $primary2, $where)
    {
        $sql = "SELECT $select FROM $table1 JOIN $table2 ON $primary2=$primary1 WHERE $where";

        return $this->db->query($sql);
    }

    public function getCustom($table)
    {
        return $this->db->from($table);
    }

    public function getDB()
    {
        return $this->db;
    }
}

/* End of file Core_m.php */

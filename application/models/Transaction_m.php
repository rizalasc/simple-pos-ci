<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Transaction_m extends CI_Model
{
    protected $table = 'sales_transaction';
    private $column_order = array(null, 'transaction_id', 'customer_name', 'total_item', 'total_price', 'is_cash', 'transaction_status'); //set column field database for datatable orderable
    private $column_search = array('transaction_id', 'customer_name', 'total_item', 'total_price', 'is_cash', 'transaction_status'); //set column field database for datatable searchable 
    private $order = array('transaction_id' => 'desc'); // default order 

    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    public function detail($transaction_id)
    {
        $transaction = $this->db->get_where($this->table, ['transaction_id' => $transaction_id])->row_array();
        $items = $this->db->select('sales_data.*,product.product_name')->from('sales_data')->join('product', 'product.product_id=sales_data.product_id')->where('sales_data.transaction_id', $transaction_id)->get()->result();
        foreach ($items as $item) {
            $item->subtotal = rupiah($item->subtotal);
        }
        $transaction['items'] = $items;

        return $transaction;
    }

    public function profitPerMonth()
    {
        $month = date("m");
        $sql = "SELECT SUM((sd.price_item-p.basic_price)*sd.quantity)as profit FROM sales_data sd join product p on p.product_id=sd.product_id where MONTH(sd.created_at)=$month ";

        return $this->db->query($sql);
    }
}

/* End of file Transaction_m.php */

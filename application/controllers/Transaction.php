<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Transaction extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("core_m", 'core');
        $this->load->model("transaction_m", 'transaction');
        $this->load->library("template");
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $this->template->load("element/template", "transaction/data_transaction");
    }

    public function create()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $this->cart->destroy();
        $data['transaction_id'] = "TRC" . strtotime(date("Y-m-d H:i:s"));
        $data['categories'] = $this->core->getFull("category");
        $this->template->load("element/template", "transaction/form", $data);
    }

    public function add_transaction()
    {
        $input = $this->input->post();
        $transaction = array(
            'transaction_id' => $input['transaction_id'],
            'customer_name' => $input['customer_name'],
            'is_cash' => $input['payment_method'],
            'total_price' => $input['total'],
            'total_item' => count($input['carts']),
            'user_id' => $this->session->user_id
        );

        $save_transaction = $this->core->insert('sales_transaction', $transaction);
        if ($save_transaction) {
            foreach ($input['carts'] as $key) {
                $data = array(
                    'transaction_id' => $input['transaction_id'],
                    'product_id' => $key['id'],
                    'category_id' => $key['category_id'],
                    'quantity' => $key['qty'],
                    'price_item' => $key['price'],
                    'subtotal' => $key['subtotal']
                );
                $save_data = $this->core->insert('sales_data', $data);
                $get_qty = $this->core->getByWhere('product', ['product_id' => $key['id']])->row_array();
                $current_qty = (int)$get_qty['product_qty'] - (int)$key['qty'];
                $update_qty = $this->core->update('product', ['product_qty' => $current_qty], ['product_id' => $key['id']]);
            }
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Membuat Transaksi";
            echo json_encode($res);
        } else {
            $res['title'] = 'Gagal';
            $res['status'] = 'error';
            $res['message'] = "Gagal Membuat Transaksi";
            echo json_encode($res);
        }
    }

    public function add_item()
    {
        $product_id = $this->input->post("product_id");
        $quantity = $this->input->post("quantity");
        $sale_price = $this->input->post("sale_price");

        // var_dump($this->input->post());
        // die;
        $get_product_detail = $this->core->getJoin2table('product.*,category_name', "product", "category", "product.category_id", "category.category_id", "product_id='" . $product_id . "'")->row_array();

        if ($get_product_detail) {
            $data = array(
                'id'      => $product_id,
                'qty'     => $quantity,
                'price'   => (int)$sale_price,
                'category_id' => $get_product_detail['category_id'],
                'category_name' => $get_product_detail['category_name'],
                'name'    => $get_product_detail['product_name'],
                'subtotal' => (int)$quantity * $sale_price
            );
            echo json_encode(
                array(
                    'status' => 'success',
                    'data' => $data,
                )
            );
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function datatable()
    {
        $list = $this->transaction->get_datatables();
        $data = array();
        foreach ($list as $a) {
            $row = array();
            $row[] = '';
            $row[] = $a->transaction_id;
            $row[] = $a->customer_name;
            $row[] = $a->total_item;
            $row[] = rupiah($a->total_price);
            $row[] = $a->is_cash == 1 ? '<span class="badge badge-pill badge-success">Cash</span>' : '<span class="badge badge-pill badge-warning">Debit</span>';
            $row[] = $a->transaction_status == 'order' ? '<span class="badge badge-pill badge-danger">Order</span>' : '<span class="badge badge-pill badge-success">Done</span>';

            $confirm = $a->transaction_status == 'order' ? '<a class="dropdown-item" href="javascript:void(0)" onclick="done_transaction(' . "'" . $a->transaction_id . "'" . ')"><i class="dw dw-checked"></i> Order Selesai</a>' : '';
            $cancel = $a->transaction_status == 'order' ? '<a class="dropdown-item" href="javascript:void(0)" onclick="cancel_transaction(' . "'" . $a->transaction_id . "'" . ')"><i class="dw dw-delete-3"></i> Batal</a>' : '';

            $row[] = '<div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    ' . $confirm . '
                    <a class="dropdown-item" href="javascript:void(0)" onclick="view_transaction(' . "'" . $a->transaction_id . "'" . ')"><i class="dw dw-eye"></i> Detail</a>
                    ' . $cancel . '
                </div>
            </div>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->transaction->count_all(),
            "recordsFiltered" => $this->transaction->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }

    public function detail($transaction_id)
    {
        $transaction = $this->transaction->detail($transaction_id);
        echo json_encode($transaction);
        exit;
    }

    public function delete($id)
    {
        $delete = $this->core->delete('sales_transaction', ['transaction_id' => $id]);
        $delete2 = $this->core->delete('sales_data', ['transaction_id' => $id]);
        if ($delete2) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Membatalkan Transaksi";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Membatalkan Transaksi";
        }
        echo json_encode($res);
        exit();
    }

    public function done($id)
    {
        $done = $this->core->update('sales_transaction', ['transaction_status' => 'done'], ['transaction_id' => $id]);
        if ($done) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Konfirmasi Transaksi";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Konfirmasi Transaksi";
        }
        echo json_encode($res);
        exit();
    }
}

/* End of file Transaction.php */

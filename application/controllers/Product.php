<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("core_m", "core");
        $this->load->model('product_m', 'product');

        $this->load->library("template");
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $data['suppliers'] = $this->core->getFull('supplier');
        $data['categories'] = $this->core->getFull('category');
        $this->template->load("element/template", "product/data_product", $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('product_name', 'Nama Produk', 'trim|required');
        $this->form_validation->set_rules('product_desc', 'Deskripsi Produk', 'trim|required');
        $this->form_validation->set_rules('product_qty', 'Kuantitas', 'trim|required');
        $this->form_validation->set_rules('basic_price', 'Harga Modal', 'trim|required');
        $this->form_validation->set_rules('sale_price', 'Harga Jual', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('category_id', 'Kategori', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $id = '';
            $prefix = 'PROD';
            $id = generate_id($prefix);

            $store = array(
                'product_id' => $id,
                'product_name' => $input['product_name'],
                'category_id' => $input['category_id'],
                'supplier_id' => $input['supplier_id'],
                'product_desc' => $input['product_desc'],
                'product_qty' => $input['product_qty'],
                'basic_price' => $input['basic_price'],
                'sale_price' => $input['sale_price'],
            );


            $save = $this->core->insert('product', $store);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Menambahkan Produk";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Menambahkan Produk";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function update()
    {
        $this->form_validation->set_rules('product_id', 'Produk ID', 'trim|required');
        $this->form_validation->set_rules('product_name', 'Nama Produk', 'trim|required');
        $this->form_validation->set_rules('product_desc', 'Deskripsi Produk', 'trim|required');
        $this->form_validation->set_rules('product_qty', 'Kuantitas', 'trim|required');
        $this->form_validation->set_rules('basic_price', 'Harga Modal', 'trim|required');
        $this->form_validation->set_rules('sale_price', 'Harga Jual', 'trim|required');
        $this->form_validation->set_rules('supplier_id', 'Supplier', 'trim|required');
        $this->form_validation->set_rules('category_id', 'Kategori', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $update = array(
                'product_name' => $input['product_name'],
                'category_id' => $input['category_id'],
                'supplier_id' => $input['supplier_id'],
                'product_desc' => $input['product_desc'],
                'product_qty' => $input['product_qty'],
                'basic_price' => $input['basic_price'],
                'sale_price' => $input['sale_price'],
            );
            $save = $this->core->update('product', $update, ['product_id' => $input['product_id']]);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Mengubah Produk";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Mengubah Produk";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function edit($id)
    {
        $data = $this->core->getByWhere('product', ['product_id' => $id]);
        $res['success'] = true;
        $res['data'] = $data->row_array();
        echo json_encode($res);
        exit();
    }

    public function delete($id)
    {
        $delete = $this->core->delete('product', ['product_id' => $id]);
        if ($delete) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Menghapus Produk";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Menghapus Produk";
        }
        echo json_encode($res);
        exit();
    }

    public function get_product()
    {
        $category_id = $this->input->post("category_id");
        $products = $this->core->getByWhere("product", ["category_id" => $category_id])->result();
        echo json_encode($products);
        exit();
    }

    public function get_price($product_id)
    {
        $products = $this->core->getByWhere("product", ['product_id' => $product_id])->row_array();
        echo json_encode($products);
        exit();
    }

    public function datatable()
    {
        $list = $this->product->get_datatables();
        $data = array();
        foreach ($list as $a) {
            $row = array();
            $row[] = '';
            $row[] = $a->product_id;
            $row[] = $a->product_name;
            $row[] = $a->category_name;
            $row[] = $a->product_desc;
            $row[] = $a->product_qty;
            $row[] = rupiah($a->sale_price);
            $row[] = $a->supplier_name;



            $row[] = '<div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <a class="dropdown-item" href="javascript:void(0)" onclick="edit_product(' . "'" . $a->product_id . "'" . ')"><i class="dw dw-edit2"></i> Ubah</a>
                    <a class="dropdown-item" href="javascript:void(0)" onclick="hapus_product(' . "'" . $a->product_id . "'" . ')"><i class="dw dw-delete-3"></i> Hapus</a>
                </div>
            </div>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->product->count_all(),
            "recordsFiltered" => $this->product->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }
}

/* End of file Product.php */

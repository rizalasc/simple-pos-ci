<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends CI_Controller
{

    public function index()
    {
    }

    public function add_item()
    {
        $product_id = $this->input->post("product_id");
        $quantity = $this->input->post("quantity");
        $sale_price = $this->input->post("sale_price");

        $get_product_detail = $this->core_m->getJoin2table('product.*,category_name', "product", "category", "product.category_id", "category.category_id", "product_id='" . $product_id . "'")->row_array();
        if ($get_product_detail) {
            $data = array(
                'id'      => $product_id,
                'qty'     => $quantity,
                'price'   => $sale_price,
                'category_id' => $get_product_detail[0]['category_id'],
                'category_name' => $get_product_detail[0]['category_name'],
                'name'    => $get_product_detail[0]['product_name']
            );
            $this->cart->insert($data);
            echo json_encode(
                array(
                    'status' => 'success',
                    'data' => $this->cart->contents(),
                    'total_item' => $this->cart->total_items(),
                    'total_price' => $this->cart->total()
                )
            );
        } else {
            echo json_encode(array('status' => 'error'));
        }
    }

    public function remove($id)
    {
        $index = $this->exists($id);
        $cart = array_values(unserialize($this->session->userdata('cart')));
        unset($cart[$index]);
        $this->session->set_userdata('cart', serialize($cart));

        // redirect('cart');
    }

    private function exists($id)
    {
        $cart = array_values(unserialize($this->session->userdata('cart')));
        for ($i = 0; $i < count($cart); $i++) {
            if ($cart[$i]['id'] == $id) {
                return $i;
            }
        }
        return -1;
    }

    private function total()
    {
        $items = array_values(unserialize($this->session->userdata('cart')));
        $s = 0;
        foreach ($items as $item) {
            $s += $item['price'] * $item['quantity'];
        }
        return $s;
    }
}

/* End of file Cart.php */

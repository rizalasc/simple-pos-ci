<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("template");
        $this->load->model("core_m", "core");
        $this->load->model('transaction_m', 'transaction');
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $data['products'] = $this->core->getCustom('product')->count_all_results();
        $data['suppliers'] = $this->core->getCustom('supplier')->count_all_results();
        $data['transactions'] = $this->core->getCustom('sales_transaction')->where(['MONTH(created_at)' => date('m')])->count_all_results();
        $data['profit'] = $this->transaction->profitPerMonth()->row_array();
        $data['years'] = $this->core->getDB()->distinct()->select('YEAR(created_at) as year')->get('sales_transaction');
        $this->template->load("element/template", "home/dashboard", $data);
    }

    public function charts()
    {
        $input = $this->input->post();

        if ($input && $input['type'] == 'all') {
            $day_count = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $produk_data = [];
            $transaksi_data = [];
            $categories = [];
            for ($i = 1; $i <= $day_count; $i++) {
                $item = $i . " " . date('F');
                array_push($categories, $item);
                $pcount = $this->core->getCustom('product')->where('DAY(created_at)', $i)->where('MONTH(created_at)', date('m'))->where('YEAR(created_at)', date('Y'))->count_all_results();
                array_push($produk_data, $pcount);
                $tcount = $this->core->getCustom('sales_transaction')->where('DAY(created_at)', $i)->where('MONTH(created_at)', date('m'))->where('YEAR(created_at)', date('Y'))->count_all_results();
                array_push($transaksi_data, $tcount);
            }
            $series = [];
            $produk = new stdClass();
            $produk->name = "Produk";
            $produk->data = $produk_data;
            array_push($series, $produk);
            $transaction = new stdClass();
            $transaction->name = "Transaksi";
            $transaction->data = $transaksi_data;
            array_push($series, $transaction);


            $res['line_chart']['series'] = $series;
            $res['line_chart']['categories'] = $categories;

            $categories_product = [];
            $series_product = [];
            $product_name = $this->core->getFull('product')->result();
            foreach ($product_name as $key) {
                array_push($categories_product, $key->product_name);
                $count = $this->core->getDB()->query("
                SELECT SUM(quantity) as qty FROM `sales_data` WHERE product_id = '$key->product_id'")->row_array();
                $qty = $count['qty'] == null ? 0 : (int)$count['qty'];
                array_push($series_product, $qty);
            }

            $res['bar_chart']['series'] = $series_product;
            $res['bar_chart']['categories'] = $categories_product;

            echo json_encode($res);
            exit;
        } else if ($input && $input['type'] == 'line_chart' && $input['month'] != 0 && $input['year'] != 0) {
            $month = $input['month'];
            $year = $input['year'];
            $day_count = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            $produk_data = [];
            $transaksi_data = [];
            $categories = [];
            for ($i = 1; $i <= $day_count; $i++) {
                $dateObj = DateTime::createFromFormat('!m', $input['month']);
                $item = $i . " " . $dateObj->format("F");
                array_push($categories, $item);
                $pcount = $this->core->getCustom('product')->where('DAY(created_at)', $i)->where('MONTH(created_at)', $month)->where('YEAR(created_at)', $year)->count_all_results();
                array_push($produk_data, $pcount);
                $tcount = $this->core->getCustom('sales_transaction')->where('DAY(created_at)', $i)->where('MONTH(created_at)', $month)->where('YEAR(created_at)', $year)->count_all_results();
                array_push($transaksi_data, $tcount);
            }
            $series = [];
            $produk = new stdClass();
            $produk->name = "Produk";
            $produk->data = $produk_data;
            array_push($series, $produk);
            $transaction = new stdClass();
            $transaction->name = "Transaksi";
            $transaction->data = $transaksi_data;
            array_push($series, $transaction);


            $res['series'] = $series;
            $res['categories'] = $categories;

            echo json_encode($res);
            exit;
        } else if ($input && $input['type'] == 'bar_chart' && $input['month'] != 0 && $input['year'] != 0) {
            $month = $input['month'];
            $year = $input['year'];
            $categories_product = [];
            $series_product = [];
            $product_name = $this->core->getFull('product')->result();
            foreach ($product_name as $key) {
                array_push($categories_product, $key->product_name);
                $count = $this->core->getDB()->query("
                SELECT SUM(quantity) as qty FROM `sales_data` WHERE product_id = '$key->product_id' AND MONTH(created_at)=$month AND YEAR(created_at)=$year")->row_array();
                $qty = $count['qty'] == null ? 0 : (int)$count['qty'];
                array_push($series_product, $qty);
            }

            $res['series'] = $series_product;
            $res['categories'] = $categories_product;

            echo json_encode($res);
            exit;
        }
    }
}

/* End of file Dashboard.php */

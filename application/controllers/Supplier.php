<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("template");
        $this->load->model("core_m", "core");
        $this->load->model("supplier_m", "supplier");
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $this->template->load("element/template", "supplier/data_supplier");
    }

    public function store()
    {
        $this->form_validation->set_rules('supplier_name', 'Nama Supplier', 'trim|required');
        $this->form_validation->set_rules('supplier_phone', 'Telepon Supplier', 'trim|required');
        $this->form_validation->set_rules('supplier_address', 'Alamat Supplier', 'trim|required');



        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $id = '';
            $prefix = 'SUPP';
            $id = generate_id($prefix);

            $store = array(
                'supplier_id' => $id,
                'supplier_name' => $input['supplier_name'],
                'supplier_phone' => $input['supplier_phone'],
                'supplier_address' => $input['supplier_address'],
            );


            $save = $this->core->insert('supplier', $store);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Menambahkan Supplier";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Menambahkan Supplier";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function update()
    {
        $this->form_validation->set_rules('supplier_id', 'Supplier ID', 'trim|required');
        $this->form_validation->set_rules('supplier_name', 'Nama Supplier', 'trim|required');
        $this->form_validation->set_rules('supplier_phone', 'Telepon Supplier', 'trim|required');
        $this->form_validation->set_rules('supplier_address', 'Alamat Supplier', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $update = array(
                'supplier_name' => $input['supplier_name'],
                'supplier_phone' => $input['supplier_phone'],
                'supplier_address' => $input['supplier_address'],
            );
            $save = $this->core->update('supplier', $update, ['supplier_id' => $input['supplier_id']]);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Mengubah Supplier";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Mengubah Supplier";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function edit($id)
    {
        $data = $this->core->getByWhere('supplier', ['supplier_id' => $id]);
        $res['success'] = true;
        $res['data'] = $data->row_array();
        echo json_encode($res);
        exit();
    }

    public function delete($id)
    {
        $delete = $this->core->delete('supplier', ['supplier_id' => $id]);
        if ($delete) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Menghapus Supplier";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Menghapus Supplier";
        }
        echo json_encode($res);
        exit();
    }

    public function datatable()
    {
        $list = $this->supplier->get_datatables();
        $data = array();
        foreach ($list as $a) {
            $row = array();
            $row[] = '';
            $row[] = $a->supplier_id;
            $row[] = $a->supplier_name;
            $row[] = $a->supplier_phone;
            $row[] = $a->supplier_address;


            $row[] = '<div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <a class="dropdown-item" href="javascript:void(0)" onclick="edit_supplier(' . "'" . $a->supplier_id . "'" . ')"><i class="dw dw-edit2"></i> Ubah</a>
                    <a class="dropdown-item" href="javascript:void(0)" onclick="hapus_supplier(' . "'" . $a->supplier_id . "'" . ')"><i class="dw dw-delete-3"></i> Hapus</a>
                </div>
            </div>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->supplier->count_all(),
            "recordsFiltered" => $this->supplier->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }
}

/* End of file Supplier.php */

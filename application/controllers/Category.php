<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("template");
        $this->load->model("core_m", "core");
        $this->load->model("category_m", "category");
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $this->template->load("element/template", "category/data_category");
    }

    public function store()
    {
        $this->form_validation->set_rules('category_name', 'Nama Kategori', 'trim|required');
        $this->form_validation->set_rules('category_desc', 'Deskripsi Kategori', 'trim|required');



        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $id = '';
            $prefix = 'KTGR';
            $id = generate_id($prefix);

            $store = array(
                'category_id' => $id,
                'category_name' => $input['category_name'],
                'category_desc' => $input['category_desc']
            );


            $save = $this->core->insert('category', $store);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Menambahkan Kategori";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Menambahkan Kategori";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function update()
    {
        $this->form_validation->set_rules('category_id', 'Kategori ID', 'trim|required');
        $this->form_validation->set_rules('category_name', 'Nama Kategori', 'trim|required');
        $this->form_validation->set_rules('category_desc', 'Deskripsi Kategori', 'trim|required');


        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $update = array(
                'category_name' => $input['category_name'],
                'category_desc' => $input['category_desc']
            );
            $save = $this->core->update('category', $update, ['category_id' => $input['category_id']]);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Mengubah Kategori";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Mengubah Kategori";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function edit($id)
    {
        $data = $this->core->getByWhere('category', ['category_id' => $id]);
        $res['success'] = true;
        $res['data'] = $data->row_array();
        echo json_encode($res);
        exit();
    }

    public function delete($id)
    {
        $delete = $this->core->delete('category', ['category_id' => $id]);
        if ($delete) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Menghapus Kategori";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Menghapus Kategori";
        }
        echo json_encode($res);
        exit();
    }

    public function datatable()
    {
        $list = $this->category->get_datatables();
        $data = array();
        foreach ($list as $a) {
            $row = array();
            $row[] = '';
            $row[] = $a->category_id;
            $row[] = $a->category_name;
            $row[] = $a->category_desc;

            $row[] = '<div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <a class="dropdown-item" href="javascript:void(0)" onclick="edit_category(' . "'" . $a->category_id . "'" . ')"><i class="dw dw-edit2"></i> Ubah</a>
                    <a class="dropdown-item" href="javascript:void(0)" onclick="hapus_category(' . "'" . $a->category_id . "'" . ')"><i class="dw dw-delete-3"></i> Hapus</a>
                </div>
            </div>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->category->count_all(),
            "recordsFiltered" => $this->category->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }
}

/* End of file Category.php */

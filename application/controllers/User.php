<?php

defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library("template");
        $this->load->model('Core_m', 'core');
        $this->load->model('User_m', 'user');
    }

    public function index()
    {
        if (!$this->session->logged_in) {
            redirect('auth');
        }
        $this->template->load("element/template", "user/data_user");
    }

    public function store()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('level', 'Level', 'required');



        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $id = '';
            $prefix = '';
            if ($input['level'] == 'admin') {
                $prefix = 'ADM';
            } else {
                $prefix = 'EMP';
            }
            $id = generate_id($prefix);

            $store = array(
                'user_id' => $id,
                'fullname' => $input['fullname'],
                'username' => $input['username'],
                'email' =>  $input['email'],
                'password' => sha1('password'),
                'level' => $input['level']
            );


            $save = $this->core->insert('user', $store);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Menambahkan User";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Menambahkan User";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function update()
    {
        $this->form_validation->set_rules('user_id', 'User ID', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('level', 'Level', 'required');


        if ($this->form_validation->run() == FALSE) {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Lengkapi Data";
            echo json_encode($res);
        } else {
            $input = $this->input->post();
            $update = array(
                'username' => $input['username'],
                'fullname' => $input['fullname'],
                'email' => $input['email'],
                'level' => $input['level'],
            );
            $save = $this->core->update('user', $update, ['user_id' => $input['user_id']]);
            if ($save) {
                $res['title'] = 'Berhasil';
                $res['status'] = 'success';
                $res['message'] = "Berhasil Mengubah User";
            } else {
                $res['title'] = 'Peringatan';
                $res['status'] = 'error';
                $res['message'] = "Gagal Mengubah User";
            }
            echo json_encode($res);
            exit();
        }
    }

    public function edit($id)
    {
        $data = $this->core->getByWhere('user', ['user_id' => $id]);
        $res['success'] = true;
        $res['data'] = $data->row_array();
        echo json_encode($res);
        exit();
    }

    public function delete($id)
    {
        $delete = $this->core->delete('user', ['user_id' => $id]);
        if ($delete) {
            $res['title'] = 'Berhasil';
            $res['status'] = 'success';
            $res['message'] = "Berhasil Menghapus User";
        } else {
            $res['title'] = 'Peringatan';
            $res['status'] = 'error';
            $res['message'] = "Gagal Menghapus User";
        }
        echo json_encode($res);
        exit();
    }

    public function datatable()
    {
        $list = $this->user->get_datatables();
        $data = array();
        foreach ($list as $a) {
            $row = array();
            $row[] = '';
            $row[] = $a->user_id;
            $row[] = $a->username;
            $row[] = $a->fullname;
            $row[] = $a->email;
            if ($a->level == 'admin') {
                $level = '<span class="badge badge-success">' . ucwords($a->level) . '</span>';
            } else {
                $level = '<span class="badge badge-info">' . ucwords($a->level) . '</span>';
            }

            $row[] = $level;

            $row[] = '<div class="dropdown">
                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <i class="dw dw-more"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                    <a class="dropdown-item" href="javascript:void(0)" onclick="edit_user(' . "'" . $a->user_id . "'" . ')"><i class="dw dw-edit2"></i> Ubah</a>
                    <a class="dropdown-item" href="javascript:void(0)" onclick="hapus_user(' . "'" . $a->user_id . "'" . ')"><i class="dw dw-delete-3"></i> Hapus</a>
                </div>
            </div>';

            $data[] = $row;
        }

        $result = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->user->count_all(),
            "recordsFiltered" => $this->user->count_filtered(),
            "data" => $data,
        );


        echo json_encode($result);
        exit();
    }
}

/* End of file User.php */

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('template');
        $this->load->model('core_m', 'core');
    }

    public function index()
    {
        $this->load->view('auth/login');
    }

    public function login()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $login['username']  = $this->input->post('username');
        $login['password']  = $this->input->post('password');


        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                                                        <strong>Form Tidak Lengkap!</strong> Harap melengkapi form.
                                                    </div>');
            redirect('auth');
        } else {
            $user = $this->core->checkUser('user', $login['username']);
            $check = $this->core->checkPassword('user', $login);
            if ($check) {
                $session = array(
                    'user_id' => $user['user_id'],
                    'username' => $user['username'],
                    'logged_in' => TRUE,
                    'level' => $user['level']
                );
                $this->session->set_userdata($session);
                // $res['status'] = 'success';
                redirect('user');
            } else {
                // $res['status'] = 'warning';
                // $res['title'] = 'Peringatan';
                // $res['message'] = 'Password Anda Salah';
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                                                    <strong>Username atau Password salah!</strong> Cek kembali akun anda.
                                                </div>');
                redirect('auth');
            }
        }
        // echo json_encode($res);
        // exit();
    }

    public function logout()
    {
        $this->session->sess_destroy();
        // $res['status'] = 'success';
        // echo json_encode($res);
        redirect('auth');
        exit();
    }
}

/* End of file Login.php */

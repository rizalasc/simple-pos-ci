<div class="min-height-200px">

    <div class="title pb-20">
        <h2 class="h3 mb-0">Dashboard</h2>
    </div>
    <div class="row pb-10">
        <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
            <div class="card-box height-100-p widget-style3">
                <div class="d-flex flex-wrap">
                    <div class="widget-data">
                        <div class="weight-700 font-24 text-dark"><?= $products; ?></div>
                        <div class="font-14 text-secondary weight-500">Produk</div>
                    </div>
                    <div class="widget-icon">
                        <div class="icon" data-color="#00eccf" style="color: rgb(0, 236, 207);"><i class="icon-copy dw dw-box"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
            <div class="card-box height-100-p widget-style3">
                <div class="d-flex flex-wrap">
                    <div class="widget-data">
                        <div class="weight-700 font-24 text-dark"><?= $suppliers; ?></div>
                        <div class="font-14 text-secondary weight-500">Supplier</div>
                    </div>
                    <div class="widget-icon">
                        <div class="icon" data-color="#ff5b5b" style="color: rgb(255, 91, 91);"><span class="icon-copy dw dw-reload"></span></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
            <div class="card-box height-100-p widget-style3">
                <div class="d-flex flex-wrap">
                    <div class="widget-data">
                        <div class="weight-700 font-24 text-dark"><?= $transactions; ?></div>
                        <div class="font-14 text-secondary weight-500">Transaksi</div>
                    </div>
                    <div class="widget-icon">
                        <div class="icon"><i class="icon-copy dw dw-switch" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-3 col-md-6 mb-20">
            <div class="card-box height-100-p widget-style3">
                <div class="d-flex flex-wrap">
                    <div class="widget-data">
                        <div class="weight-700 font-24 text-dark"><?= rupiah($profit['profit']); ?></div>
                        <div class="font-14 text-secondary weight-500">Profit</div>
                    </div>
                    <div class="widget-icon">
                        <div class="icon" data-color="#09cc06" style="color: rgb(9, 204, 6);"><i class="icon-copy fa fa-money" aria-hidden="true"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="col-md-12">
            <div class="row mb-20">
                <div class="col-md-6">
                    <div class="h5 mb-md-0">Transaksi & Produk</div>
                </div>
                <div class="col-md-6 text-right">
                    <select class="form-control col-md-3 form-control-sm selectpicker" name="select-month">
                        <option value="0" selected>- Pilih Bulan -</option>
                        <?php for ($m = 1; $m <= 12; ++$m) : ?>
                            <option value="<?= $m; ?>"><?= date('F', mktime(0, 0, 0, $m, 1)); ?></option>
                        <?php endfor; ?>
                    </select>
                    <select class="form-control col-md-3 form-control-sm selectpicker" name="select-year">
                        <option value="0" selected>- Pilih Tahun -</option>
                        <?php foreach ($years->result() as $year) : ?>
                            <option value="<?= $year->year; ?>"><?= $year->year; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <button type="button" id="filter-chart" class="btn btn-sm btn-primary">Filter</button>
                </div>
            </div>
            <div id="activities-chart"></div>
        </div>
    </div>
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <div class="col-md-12">
            <div class="row mb-20">
                <div class="col-md-6">
                    <div class="h5 mb-md-0">Grafik Penjualan Produk</div>
                </div>
                <div class="col-md-6 text-right">
                    <select class="form-control col-md-3 form-control-sm selectpicker" name="select-month2">
                        <option value="0" selected>- Pilih Bulan -</option>
                        <?php for ($m = 1; $m <= 12; ++$m) : ?>
                            <option value="<?= $m; ?>"><?= date('F', mktime(0, 0, 0, $m, 1)); ?></option>
                        <?php endfor; ?>
                    </select>
                    <select class="form-control col-md-3 form-control-sm selectpicker" name="select-year2">
                        <option value="0" selected>- Pilih Tahun -</option>
                        <?php foreach ($years->result() as $year) : ?>
                            <option value="<?= $year->year; ?>"><?= $year->year; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <button type="button" id="filter-chart2" class="btn btn-sm btn-primary">Filter</button>
                </div>
            </div>
            <div id="popular-product-chart"></div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $.ajax({
            url: `${base_url}dashboard/charts`,
            type: "POST",
            data: {
                type: 'all'
            },
            dataType: "JSON",
            success: function(res, status) {
                if (status == "success") {
                    // console.log(res);
                    drawChart(res.line_chart.series, res.line_chart.categories);
                    chartProduct(res.bar_chart.series, res.bar_chart.categories);
                }
            }
        });


    });

    $('#filter-chart').on('click', function(e) {
        e.preventDefault();
        const month = $('[name=select-month]').val();
        const year = $('[name=select-year]').val();
        $.ajax({
            url: `${base_url}dashboard/charts`,
            type: "POST",
            data: {
                month: parseInt(month),
                year: parseInt(year),
                type: 'line_chart'
            },
            dataType: "JSON",
            success: function(res, status) {
                if (status == "success") {
                    redrawChart(res.series, res.categories);
                }
            }
        })
    });

    $('#filter-chart2').on('click', function(e) {
        e.preventDefault();
        const month = $('[name=select-month2]').val();
        const year = $('[name=select-year2]').val();
        $.ajax({
            url: `${base_url}dashboard/charts`,
            type: "POST",
            data: {
                month: parseInt(month),
                year: parseInt(year),
                type: 'bar_chart'
            },
            dataType: "JSON",
            success: function(res, status) {
                if (status == "success") {
                    chartProduct(res.series, res.categories);
                }
            }
        })
    })
</script>
<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    <h4>Tambah Transaksi</h4>
                </div>
                <!-- <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">blank</li>
                    </ol>
                </nav> -->
            </div>
        </div>
    </div>
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <form id="form-transaction">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Kode Transaksi</label>
                        <input type="text" name="transaction_id" disabled class="form-control" value="<?= $transaction_id; ?>">
                    </div>
                    <div class="form-group">
                        <label for="">Nama Pembeli</label>
                        <input type="text" name="customer_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Metode Pembayaran</label>
                        <select name="payment_method" class="form-control" id="">
                            <option value="" selected disabled>- Pilih Metode -</option>
                            <option value="1">Cash</option>
                            <option value="2">Debit</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Kategori</th>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga Satuan</th>
                            <th>Input Produk</th>
                        </tr>
                    </thead>
                    <tbody id="transaksi-item">
                        <tr>
                            <td>
                                <select name="category_id" id="transaksi_category_id" class="form-control">
                                    <option value="">- Pilih -</option>
                                    <?php foreach ($categories->result() as $c) : ?>
                                        <option value="<?= $c->category_id; ?>"><?= $c->category_name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <select name="product_id" id="transaksi_product_id" class="form-control">

                                </select>
                            </td>
                            <td>
                                <input type="number" name="jumlah" id="jumlah" min="1" value="1" class="form-control">
                            </td>
                            <td>
                                <input type="text" name="sale_price" id="sale_price" class="form-control" disabled>
                            </td>
                            <td>
                                <button type="button" class="btn btn-primary" id="tambah-barang">
                                    <i class="icon-copy fa fa-cart-plus" aria-hidden="true"></i>
                                </button>
                            </td>
                        </tr>
                        <?php if (!empty($carts) && is_array($carts)) { ?>
                            <?php foreach ($carts['data'] as $k => $cart) { ?>
                                <tr id="<?php echo $k; ?>" class="cart-value">
                                    <td><?php echo $cart['category_name']; ?></td>
                                    <td><?php echo $cart['name']; ?></td>
                                    <td><?php echo $cart['qty']; ?></td>
                                    <td>Rp<?php echo number_format($cart['price']); ?></td>
                                    <td><span class="btn btn-danger btn-sm transaksi-delete-item" data-cart="<?php echo $k; ?>">x</span></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>Total Penjualan</td>
                            <td id="total-pembelian"><?php echo !empty($carts) ? 'Rp' . number_format($carts['total_price']) : ''; ?></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <button type="button" id="btn-clear" class="btn btn-sm btn-danger">
                        Hapus
                    </button>
                    <button type="submit" id="btn-submit" class="btn btn-sm btn-success">
                        Buat Transaksi
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>
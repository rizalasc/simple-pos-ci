<div class="left-side-bar">
    <div class="brand-logo">
        <a href="<?= site_url(); ?>">
            <!-- <img src="<?= base_url('public/plugins/deskapp-master/'); ?>vendors/images/deskapp-logo.svg" alt="" class="dark-logo">
            <img src="<?= base_url('public/plugins/deskapp-master/'); ?>vendors/images/deskapp-logo-white.svg" alt="" class="light-logo"> -->
            <h4 style="color: white;">Point Of Sale</h4>
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                <li>
                    <a href="<?= base_url('dashboard');; ?>" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-analytics1"></span><span class="mtext">Dashboard</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="micon dw dw-folder"></span><span class="mtext">Master Data</span>
                    </a>
                    <ul class="submenu">
                        <!-- <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-option="on">
                                <span class="micon fa fa-folder"></span><span class="mtext">FAQ</span>
                            </a>
                            <ul class="submenu child" style="display: block;">
                                <li><a href="<?= base_url('category-faq'); ?>">Category FAQ</a></li>
                                <li><a href="<?= base_url('faq'); ?>">Data FAQ</a></li>
                            </ul>
                        </li> -->
                        <li>
                            <a href="<?= base_url('category'); ?>" class="dropdown-toggle no-arrow">
                                <span class="micon fa fa-folder"></span><span class="mtext">Data Kategori</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('product'); ?>" class="dropdown-toggle no-arrow">
                                <span class="micon fa fa-folder"></span><span class="mtext">Data Produk</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('supplier'); ?>" class="dropdown-toggle no-arrow">
                                <span class="micon fa fa-folder"></span><span class="mtext">Data Supplier</span>
                            </a>
                        </li>
                        <?php if ($this->session->level == 'admin') : ?>
                            <li>
                                <a href="<?= base_url('user'); ?>" class="dropdown-toggle no-arrow">
                                    <span class="micon fa fa-folder"></span><span class="mtext">Data User</span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <!-- <li><a href="index.html">Dashboard style 1</a></li>
                        <li><a href="index2.html">Dashboard style 2</a></li>
                        <li><a href="index3.html">Dashboard style 3</a></li> -->
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="micon dw dw-switch"></span><span class="mtext">Data Transaksi</span>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="<?= base_url('transaction'); ?>" class="dropdown-toggle no-arrow">
                                <span class="micon dw dw-list"></span><span class="mtext">List Transaksi</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url('transaction/create'); ?>" class="dropdown-toggle no-arrow">
                                <span class="micon dw dw-add"></span><span class="mtext">Buat Transaksi</span>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#" class="dropdown-toggle no-arrow">
                                <span class="micon ion-checkmark-round"></span><span class="mtext">Approved</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle no-arrow">
                                <span class="micon ion-android-settings"></span><span class="mtext">Setup</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle no-arrow">
                                <span class="micon ion-android-done-all"></span><span class="mtext">Active</span>
                            </a>
                        </li> -->
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
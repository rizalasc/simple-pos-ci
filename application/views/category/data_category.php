<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    <h4>Data Kategori</h4>
                </div>
                <!-- <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">blank</li>
                    </ol>
                </nav> -->
            </div>
            <div class="col-md-6 col-sm-12 text-right">
                <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#category-modal" type="button">Tambah</a>
            </div>
        </div>
    </div>
    <div class="card-box mb-30">
        <div class="pd-20">
            <!-- <h4 class="text-blue h4">Data Table with Checckbox select</h4> -->
            <!-- <div class="btn-list">
                <button type="button" class="btn btn-outline-primary">Primary</button>
                <button type="button" class="btn btn-outline-secondary">Secondary</button>
                <button type="button" class="btn btn-outline-success">Success</button>
                <button type="button" class="btn btn-outline-danger">Danger</button>
                <button type="button" class="btn btn-outline-warning">Warning</button>
                <button type="button" class="btn btn-outline-info">Info</button>
                <button type="button" class="btn btn-outline-light">Light</button>
                <button type="button" class="btn btn-outline-dark">Dark</button>
            </div> -->
        </div>
        <div class="pb-20">
            <table class="data-table table stripe hover nowrap" id="table-category">
                <thead>
                    <tr>
                        <th>
                            No
                        </th>
                        <th>Kategori ID</th>
                        <th>Nama Kategori</th>
                        <th>Deskripsi Kategori</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-lg" id="category-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="category-modal-label">Tambah Kategori</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="POST" id="category-form">
                <div class="modal-body">
                    <input type="hidden" name="category_id">
                    <div class="form-group">
                        <label>Nama Kategori</label>
                        <input type="text" name="category_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Kategori</label>
                        <textarea name="category_desc" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    var table;
    $(document).ready(function() {
        table = $('#table-category').DataTable({
            "destroy": true,
            "processing": true, //Feature control the processing indicator.
            "lengthChange": true,
            "serverSide": true,
            "order": [],
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('category/datatable'); ?>",
                "type": "POST"
            },
            "columnDefs": [{
                "targets": [-1],
                "orderable": false,
            }, ],
        });
        table.on('draw.dt', function(data, type, row) {
            var PageInfo = $('#table-category').DataTable().page.info();
            table.column(0, {
                page: 'current'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1 + PageInfo.start;
            });
        });
    });
</script>
<?php

function generate_id($prefix)
{
    $id = $prefix . strtotime(date("Y-m-d H:i:s"));
    return $id;
}

function rupiah($angka)
{

    $hasil_rupiah = "Rp " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
}

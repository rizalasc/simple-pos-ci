# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.14-MariaDB)
# Database: db_pos
# Generation Time: 2021-12-30 09:00:38 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `category_id` varchar(100) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_desc` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;

INSERT INTO `category` (`category_id`, `category_name`, `category_desc`, `created_at`)
VALUES
	('KTGR1640793865','Minuman','Beraneka minuman','2021-12-29 23:04:25'),
	('KTGR1640793878','Makanan','Beraneka makanan','2021-12-29 23:04:38');

/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table product
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` varchar(100) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `category_id` varchar(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_qty` int(11) NOT NULL,
  `basic_price` int(20) NOT NULL,
  `sale_price` int(20) NOT NULL,
  `tax_price` int(20) DEFAULT NULL,
  `subtotal` int(100) NOT NULL,
  `supplier_id` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`product_id`),
  KEY `category_id` (`category_id`),
  KEY `product_supplier` (`supplier_id`),
  CONSTRAINT `product_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_supplier` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;

INSERT INTO `product` (`product_id`, `product_name`, `category_id`, `product_desc`, `product_qty`, `basic_price`, `sale_price`, `tax_price`, `subtotal`, `supplier_id`, `created_at`)
VALUES
	('PROD091312321','Hot Chocho','KTGR1640793865','Hot Chocho',8,15000,17000,NULL,17000,'SUPP12093213','2021-12-29 23:07:11'),
	('PROD091312322','Vanilla Latte','KTGR1640793865','Vanilla Latte',8,18000,20000,NULL,20000,'SUPP12093213','2021-11-29 23:07:11');

/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sales_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_data`;

CREATE TABLE `sales_data` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `category_id` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `price_item` int(100) NOT NULL,
  `subtotal` int(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `salesdata_product` (`product_id`),
  KEY `salesdata_category` (`category_id`),
  KEY `salesdata_transaction` (`transaction_id`),
  CONSTRAINT `salesdata_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `salesdata_product` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `salesdata_transaction` FOREIGN KEY (`transaction_id`) REFERENCES `sales_transaction` (`transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `sales_data` WRITE;
/*!40000 ALTER TABLE `sales_data` DISABLE KEYS */;

INSERT INTO `sales_data` (`id`, `transaction_id`, `product_id`, `category_id`, `quantity`, `price_item`, `subtotal`, `created_at`)
VALUES
	(1,'TRC1640839227','PROD091312321','KTGR1640793865',2,17000,34000,'2021-12-30 11:41:06'),
	(2,'TRC1640839227','PROD091312322','KTGR1640793865',2,20000,40000,'2021-12-30 11:41:06');

/*!40000 ALTER TABLE `sales_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sales_transaction
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sales_transaction`;

CREATE TABLE `sales_transaction` (
  `transaction_id` varchar(100) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `is_cash` tinyint(1) NOT NULL COMMENT '1: Cash, 0: TF/Debit',
  `total_price` int(100) NOT NULL,
  `total_item` int(100) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `transaction_status` enum('order','done') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_user` (`user_id`),
  CONSTRAINT `transaction_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `sales_transaction` WRITE;
/*!40000 ALTER TABLE `sales_transaction` DISABLE KEYS */;

INSERT INTO `sales_transaction` (`transaction_id`, `customer_name`, `is_cash`, `total_price`, `total_item`, `user_id`, `transaction_status`, `created_at`)
VALUES
	('TRC1640839227','Rizal Mutaqin',1,74000,2,'ADM202101231201','done','2021-12-30 11:41:06');

/*!40000 ALTER TABLE `sales_transaction` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table supplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `supplier_id` varchar(100) NOT NULL,
  `supplier_name` varchar(255) NOT NULL,
  `supplier_phone` varchar(50) NOT NULL,
  `supplier_address` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `supplier` WRITE;
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;

INSERT INTO `supplier` (`supplier_id`, `supplier_name`, `supplier_phone`, `supplier_address`, `created_at`)
VALUES
	('SUPP12093213','Toko A','1232132312','Bandung','2021-12-29 23:06:35');

/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` varchar(100) NOT NULL,
  `username` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo_profile` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('admin','employee') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `username`, `fullname`, `email`, `photo_profile`, `password`, `level`)
VALUES
	('ADM202101231201','admin','Administrator','administrator@asch.com',NULL,'d033e22ae348aeb5660fc2140aec35850c4da997','admin');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

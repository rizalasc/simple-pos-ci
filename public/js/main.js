var $el = $("body");
$("#user-modal").on('hidden.bs.modal', function() {
    $('#user-modal-label').text('Tambah User');
    $('#user-form')[0].reset();
});

$('form#user-form').submit(function(e) {
    e.preventDefault();
    let url = '';
    let status = '';

    let data = $(this).serializeArray();
    // console.log(data);
    if ($('[name="user_id"]').val() == "") {
        url = 'store';
        status = 'add';
    } else {
        url = 'update';
        status = 'edit';
    }

    $.ajax({
        url: `${base_url}user/${url}`,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(a) {
            swal(`${a.title}`, `${a.message}`, `${a.status}`);
            if (a.status == "success") {
                $('#user-modal').modal('hide');
                $('[name="user_id"]').val('')
                $('#user-form')[0].reset();
                table.ajax.reload();
            }
            $('#user-modal-label').text('Tambah User');
        }
    })
});

function edit_user(user) {
    $('#user-modal-label').text('Ubah User');
    $.ajax({
        url: `${base_url}user/edit/${user}`,
        type: "GET",
        dataType: "JSON",
        success: function(a) {
            if (a.success == true) {
                let data = a.data;
                $('[name="user_id"]').val(data.user_id);
                $('[name="username"]').val(data.username);
                $('[name="fullname"]').val(data.fullname);
                $('[name="email"]').val(data.email);
                $('[name="level"]').val(data.level);
                $('#user-modal').modal('show');
            }
        }
    });
}

function hapus_user(user) {
    swal({
            title: "Apa anda yakin?",
            text: "Anda akan menghapus data user ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `${base_url}user/delete/${user}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function(a) {
                        swal(`${a.title}`, `${a.message}`, `${a.status}`);
                        if (a.status == "success") {
                            table.ajax.reload();
                        }
                    }
                });
            }
        });

}

$("#category-modal").on('hidden.bs.modal', function() {
    $('#category-modal-label').text('Tambah Category');
    $('#category-form')[0].reset();
});

$('form#category-form').submit(function(e) {
    e.preventDefault();
    let url = '';
    let status = '';

    let data = $(this).serializeArray();
    // console.log(data);
    if ($('[name="category_id"]').val() == "") {
        url = 'store';
        status = 'add';
    } else {
        url = 'update';
        status = 'edit';
    }

    $.ajax({
        url: `${base_url}category/${url}`,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(a) {
            swal(`${a.title}`, `${a.message}`, `${a.status}`);
            if (a.status == "success") {
                $('#category-modal').modal('hide');
                $('[name="category_id"]').val('')
                $('#category-form')[0].reset();
                table.ajax.reload();
            }
            $('#category-modal-label').text('Tambah category');
        }
    })
});

function edit_category(category) {
    $('#category-modal-label').text('Ubah Category');
    $.ajax({
        url: `${base_url}category/edit/${category}`,
        type: "GET",
        dataType: "JSON",
        success: function(a) {
            if (a.success == true) {
                let data = a.data;
                $('[name="category_id"]').val(data.category_id);
                $('[name="category_name"]').val(data.category_name);
                $('[name="category_desc"]').val(data.category_desc);
                $('#category-modal').modal('show');
            }
        }
    });
}

function hapus_category(category) {
    swal({
            title: "Apa anda yakin?",
            text: "Anda akan menghapus data category ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `${base_url}category/delete/${category}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function(a) {
                        swal(`${a.title}`, `${a.message}`, `${a.status}`);
                        if (a.status == "success") {
                            table.ajax.reload();
                        }
                    }
                });
            }
        });

}

//
$("#supplier-modal").on('hidden.bs.modal', function() {
    $('#supplier-modal-label').text('Tambah supplier');
    $('#supplier-form')[0].reset();
});

$('form#supplier-form').submit(function(e) {
    e.preventDefault();
    let url = '';
    let status = '';

    let data = $(this).serializeArray();
    // console.log(data);
    if ($('[name="supplier_id"]').val() == "") {
        url = 'store';
        status = 'add';
    } else {
        url = 'update';
        status = 'edit';
    }

    $.ajax({
        url: `${base_url}supplier/${url}`,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(a) {
            swal(`${a.title}`, `${a.message}`, `${a.status}`);
            if (a.status == "success") {
                $('#supplier-modal').modal('hide');
                $('[name="supplier_id"]').val('')
                $('#supplier-form')[0].reset();
                table.ajax.reload();
            }
            $('#supplier-modal-label').text('Tambah Supplier');
        }
    })
});

function edit_supplier(supplier) {
    $('#supplier-modal-label').text('Ubah supplier');
    $.ajax({
        url: `${base_url}supplier/edit/${supplier}`,
        type: "GET",
        dataType: "JSON",
        success: function(a) {
            if (a.success == true) {
                let data = a.data;
                $('[name="supplier_id"]').val(data.supplier_id);
                $('[name="supplier_name"]').val(data.supplier_name);
                $('[name="supplier_phone"]').val(data.supplier_phone);
                $('[name="supplier_address"]').val(data.supplier_address);
                $('#supplier-modal').modal('show');
            }
        }
    });
}

function hapus_supplier(supplier) {
    swal({
            title: "Apa anda yakin?",
            text: "Anda akan menghapus data supplier ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `${base_url}supplier/delete/${supplier}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function(a) {
                        swal(`${a.title}`, `${a.message}`, `${a.status}`);
                        if (a.status == "success") {
                            table.ajax.reload();
                        }
                    }
                });
            }
        });

}

$('form#product-form').submit(function(e) {
    e.preventDefault();
    let url = '';
    let status = '';

    let data = $(this).serializeArray();
    // console.log(data);
    if ($('[name="product_id"]').val() == "") {
        url = 'store';
        status = 'add';
    } else {
        url = 'update';
        status = 'edit';
    }

    $.ajax({
        url: `${base_url}product/${url}`,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(a) {
            swal(`${a.title}`, `${a.message}`, `${a.status}`);
            if (a.status == "success") {
                $('#product-modal').modal('hide');
                $('[name="product_id"]').val('')
                $('#product-form')[0].reset();
                table.ajax.reload();
            }
            $('#product-modal-label').text('Tambah Produk');
        }
    })
});

function edit_product(product) {
    $('#product-modal-label').text('Ubah Produk');
    $.ajax({
        url: `${base_url}product/edit/${product}`,
        type: "GET",
        dataType: "JSON",
        success: function(a) {
            if (a.success == true) {
                let data = a.data;
                $('[name="product_id"]').val(data.product_id);
                $('[name="product_name"]').val(data.product_name);
                $('[name="category_id"]').val(data.category_id);
                $('[name="supplier_id"]').val(data.supplier_id);
                $('[name="product_desc"]').val(data.product_desc);
                $('[name="product_qty"]').val(data.product_qty);
                $('[name="basic_price"]').val(data.basic_price);
                $('[name="sale_price"]').val(data.sale_price);
                $('#product-modal').modal('show');
            }
        }
    });
}

function hapus_product(product) {
    swal({
            title: "Apa anda yakin?",
            text: "Anda akan menghapus data produk ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `${base_url}product/delete/${product}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function(a) {
                        swal(`${a.title}`, `${a.message}`, `${a.status}`);
                        if (a.status == "success") {
                            table.ajax.reload();
                        }
                    }
                });
            }
        });

}



function view_transaction(transaction){
    $.ajax({
        url: `${base_url}transaction/detail/${transaction}`,
        type: "GET",
        dataType: "JSON",
        success: function(data,status){
            var output = '';
            if (status == "success") {
                output += `<tr>
                    <th>Kode Transaksi :</th>
                    <td>${data.transaction_id}</td>
                </tr>
                <tr>
                    <th>Nama Pembeli :</th>
                    <td>${data.customer_name}</td>
                </tr>
                <tr>
                    <th>Item :</th>
                </tr>
                <tr>
                    <th>No</th>
                    <th>Nama Item</th>
                    <th>Kuantitas</th>
                    <th>Harga</th>
                </tr>`

                $.each(data.items,function(key,value){
                    output += `<tr>
                                    <td>${key+1}</td>
                                    <td>${value.product_name}</td>
                                    <td>${value.quantity}</td>
                                    <td>${value.subtotal}</td>
                                </tr>`
                });

                $('#table-detail').html(output);
                $('#detail-modal').modal('show');
            }
        },
        error: function(){
            alert('Something Error');
        }
    })
}

function cancel_transaction(transaction) {
    swal({
            title: "Apa anda yakin?",
            text: "Anda akan membatalkan transaksi ini",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: `${base_url}transaction/delete/${transaction}`,
                    type: "POST",
                    dataType: "JSON",
                    success: function(a) {
                        swal(`${a.title}`, `${a.message}`, `${a.status}`);
                        if (a.status == "success") {
                            table.ajax.reload();
                        }
                    }
                });
            }
        });

}

function done_transaction(transaction){
    $.ajax({
        url: `${base_url}transaction/done/${transaction}`,
        type: "POST",
        dataType: "JSON",
        success: function(a) {
            swal(`${a.title}`, `${a.message}`, `${a.status}`);
            if (a.status == "success") {
                table.ajax.reload();
            }
        }
    });
}


// Transaction

$('#transaksi_category_id').on('change',function(e){
    e.preventDefault();
    $.ajax({
        url: `${base_url}product/get_product`,
        type: "POST",
        data: {
            category_id: this.value
        },
        dataType: "JSON",
        success: function(data,status) {
            if (status == "success") {
                $("#transaksi_product_id").val("");
                $("#sale_price").val("");
                var select = '';
                $.each(data, function(key,value){
                    var default_value = '';
                    if(key == 0){
                        var default_value = '<option value="">- Pilih -</option>';
                        select += default_value;
                    }
                    var opt_value = '<option value="'+value.product_id+'">'+value.product_name+'</option>';
                    select += opt_value;
                });
                $('#transaksi_product_id').html(select);
            }
        }
    })
});

$('#transaksi_product_id').on("change",function(e){
    e.preventDefault();
    $.ajax({
        url: `${base_url}product/get_price/${this.value}`,
        type: "GET",
        dataType: "JSON",
        success: function(data,status) {
            if (status == "success") {
                $("#sale_price").val(data.subtotal);
            }
        }
    })
});

var carts = [];

$("#tambah-barang").on("click",function(e){
    e.preventDefault();

    var product_id = $("#transaksi_product_id").val();
    var quantity = $("#jumlah").val();
    var sale_price = $("#sale_price").val();
    if($('#harga_satuan_net').length){
        sale_price = $('#harga_satuan_net').unmask();
    }
    if(product_id !== null && sale_price !== null){
        $.ajax({
            url: `${base_url}transaction/add_item`,
            data: {
                'product_id' : product_id,
                'quantity' : quantity,
                'sale_price' : sale_price
            },
            type: 'POST',
            dataType: "JSON",
            success: function(res){
                // var res = $.parseJSON(data);
                // console.log(data);
                var foundIndex = carts.findIndex(x => x.id === res.data.id);
                if (foundIndex >= 0) {
                    carts[foundIndex] = res.data;
                } else {
                    carts.push(res.data);
                }
                // if (carts.length > 0) {
                //     carts.forEach((element,index)=>{
                //         if (element.id === res.data.id) {
                //             carts[index] = res.data;
                //         }
                //     })
                // } else {
                //     carts.push(res.data);
                // }
                // carts.push(res.data);
                // $(".cart-value").remove();
                // // console.log(carts);
                // $.each(carts, function(key,value) {
                //     var row_2 = "";
                //     if($('#harga_satuan_net').length){
                //         // row_2 = "colspan='2'";
                //     }
                //     var display = '<tr class="cart-value" id="'+ key +'">' +
                //                 '<td>'+ value.category_name +'</td>' +
                //                 '<td>'+ value.name +'</td>' +
                //                 '<td>'+ value.qty +'</td>' +
                //                 '<th '+row_2+'>Rp'+ price(value.subtotal) +'</th>' +
                //                 '<td><span class="btn btn-danger btn-sm transaksi-delete-item" onclick="remove('+key+')" data-cart="'+ key +'">x</span></td>' +
                //                 '</tr>';
                //     $("#transaksi-item tr:last").after(display);
                // });
                // $("#total-pembelian").text('Rp'+price(carts.sum("price")));
                // $("#transaksi-item").find("input[type=text], input[type=number]").val("0");
                drawCart();
                clearRow();
            },
            error: function(){
                alert('Something Error');
            }
        });
    }else{
        alert("Silahkan isi semua box");
    }
});

function clearRow()
{
    $('#transaksi_category_id').val("");
    $('#transaksi_product_id').val("");
    $('#jumlah').val(1);
    $('#sale_price').val(0);

}

function remove(index){
    carts.splice(index,1);
    drawCart();
    clearRow();
}

function clear()
{
    $('form')[0].reset();
    carts = [];
    drawCart();
}

$('#btn-clear').on('click',function(){
    clear();
});

$('#form-transaction').on('submit',function(e){
    e.preventDefault();
    const transaction_id = $('[name=transaction_id]').val();
    const customer_name = $('[name=customer_name]').val();
    const payment_method = $('[name=payment_method]').val();

    $.ajax({
        url: `${base_url}transaction/add_transaction`,
        data: {
            'transaction_id' : transaction_id,
            'customer_name' : customer_name,
            'payment_method' : payment_method,
            'carts' : carts,
            'total' : carts.sum("subtotal")
        },
        type: 'POST',
        dataType: "JSON",
        success:function(res){
            swal(`${res.title}`,`${res.message}`,`${res.status}`);
            drawCart();
            clearRow();
            clear();
        },
        error: function(){
            alert('Something Error');
        }
    })
});

function drawCart()
{
    $(".cart-value").remove();
    // console.log(carts);
    $.each(carts, function(key,value) {
        var row_2 = "";
        if($('#harga_satuan_net').length){
            // row_2 = "colspan='2'";
        }
        var display = '<tr class="cart-value" id="'+ key +'">' +
                    '<td>'+ value.category_name +'</td>' +
                    '<td>'+ value.name +'</td>' +
                    '<td>'+ value.qty +'</td>' +
                    '<th '+row_2+'>Rp'+ price(value.subtotal) +'</th>' +
                    '<td><span class="btn btn-danger btn-sm transaksi-delete-item" onclick="remove('+key+')" data-cart="'+ key +'">x</span></td>' +
                    '</tr>';
        $("#transaksi-item tr:last").after(display);
    });
    $("#total-pembelian").text('Rp'+price(carts.sum("subtotal")));
    $("#transaksi-item").find("input[type=text], input[type=number]").val("0");
}

function price(input){
    return (input).formatMoney(0, ',', ',');
}
Number.prototype.formatMoney = function(c, d, t){
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}